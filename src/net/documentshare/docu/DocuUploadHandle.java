package net.documentshare.docu;

import java.util.HashMap;

import net.documentshare.documentconfig.DocumentConfigMgr;
import net.documentshare.documentconfig.StorageBean;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.event.TableEntityAdapter;
import net.simpleframework.core.id.ID;
import net.simpleframework.organization.IJob;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.IMultipartFile;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.swfupload.AbstractSwfUploadHandle;

import org.apache.commons.io.FilenameUtils;

public class DocuUploadHandle extends AbstractSwfUploadHandle {

	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("jobUpload".equals(beanProperty)) {
			return IJob.sj_account_normal;
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	@Override
	public void upload(final ComponentParameter compParameter, final IMultipartFile multipartFile, final HashMap<String, Object> json) {
		try {
			final String fileName = multipartFile.getOriginalFilename();
			final String extension = StringUtils.getFilenameExtension(fileName);
			final ID userId = ItSiteUtil.getLoginUser(compParameter).getId();
			final String path1 = DocuUtils.getDatabase(userId);
			// 保存文件，解压文件，转换文件
			try {
				//视频默认存在d0目录下。因为加载的问题。
				//在此处需要检查磁盘

				final String key = DocumentConfigMgr.getDocuMgr().getUsableDatabase();
				final StorageBean storageBean = DocumentConfigMgr.getDocuMgr().getStorageMap().get(key);
				if (storageBean.isFileExists(path1, fileName)) {
					return;
				}
				// 上传文件
				storageBean.uploadFile(multipartFile.getInputStream(), path1, fileName);
				final DocuBean docuBean = new DocuBean();
				docuBean.setTitle(FilenameUtils.getBaseName(fileName));
				docuBean.setExtension(extension);
				docuBean.setUserId(userId);// 上传者
				docuBean.setFileName(fileName);
				docuBean.setPath1(path1);// 存储路径
				docuBean.setPath2(key);// 存储路径
				docuBean.setStatus(EDocuStatus.edit);
				docuBean.setFileSize(multipartFile.getSize());
				docuBean.setDocuFunction(whichOne(extension.toLowerCase()));
				DocuUtils.applicationModule.doUpdate(docuBean, new TableEntityAdapter() {
					@Override
					public void afterInsert(ITableEntityManager manager, Object[] objects) {
						json.put("id", docuBean.getId().toString());
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public static final String[] docuExts = { "doc", "docx", "ppt", "pptx", "xls", "xlsx", "pdf", "wps", "rtf", "xml", "txt" };
	public static final String[] videoExts = { "mp4", "flv" };
	public static final String[] rarExts = { "rar", "zip" };

	/**
	 * 是否是视频 
	 * @return
	 */
	private boolean isVideo(final String extension) {
		for (final String e : videoExts) {
			if (e.equals(extension)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 查看是不是文档
	 * 
	 * @param extension
	 * @return
	 */
	private EDocuFunction whichOne(final String extension) {
		for (final String e : docuExts) {
			if (e.equals(extension)) {
				return EDocuFunction.docu;
			}
		}
		for (final String e : videoExts) {
			if (e.equals(extension)) {
				return EDocuFunction.data;
			}
		}
		for (final String e : rarExts) {
			if (e.equals(extension)) {
				return EDocuFunction.data;
			}
		}
		return EDocuFunction.data;
	}

}
