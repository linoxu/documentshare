package net.documentshare.docu;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.content.ContentUtils;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.organization.IJob;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractTablePagerData;
import net.simpleframework.web.page.component.ui.pager.db.AbstractDbTablePagerHandle;

public class DocuDeledTablePaperHandle extends AbstractDbTablePagerHandle {
	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("jobExecute".equals(beanProperty)) {
			return IJob.sj_manager;
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(ComponentParameter compParameter) {
		final ITableEntityManager tMgr = DocuUtils.applicationModule.getDataObjectManager(DocuBean.class);
		final List<Object> ol = new ArrayList<Object>();
		final StringBuffer sql = new StringBuffer();
		sql.append("(status=? or status=?)");
		ol.add(EDocuStatus.delete_user);
		ol.add(EDocuStatus.delete_manager);
		return tMgr.query(new ExpressionValue(sql.toString(), ol.toArray(new Object[] {})), DocuBean.class);
	}

	@Override
	public AbstractTablePagerData createTablePagerData(final ComponentParameter compParameter) {
		return new AbstractTablePagerData(compParameter) {

			@Override
			protected Map<Object, Object> getRowData(final Object arg0) {
				final DocuBean deled = (DocuBean) arg0;
				final Map<Object, Object> rowData = new LinkedHashMap<Object, Object>();
				rowData.put("title", deled.getTitle());
				rowData.put("userId", ContentUtils.getAccountAware().wrapAccountHref(compParameter, deled.getUserId(), deled.getUserText()));
				rowData.put("status", deled.getStatus()==EDocuStatus.delete_user ? "用户操作" : "管理员操作");
				rowData.put("action", "<a class='dellogB down_menu_image'></a>");
				return rowData;
			}
		};
	}

	@Override
	public Map<String, Object> getFormParameters(ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, "userId");
		return parameters;
	}

}
