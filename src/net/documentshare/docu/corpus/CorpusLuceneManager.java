package net.documentshare.docu.corpus;

import java.io.File;
import java.io.IOException;

import net.documentshare.docu.DocuUtils;
import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.lucene.LuceneQuery;
import net.simpleframework.content.EContentStatus;
import net.simpleframework.content.AbstractContentLuceneManager;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.core.bean.IDataObjectBean;
import net.simpleframework.util.BeanUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ado.IDbComponentHandle;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.search.FieldCacheTermsFilter;
import org.apache.lucene.search.FilteredQuery;
import org.apache.lucene.search.Query;

public class CorpusLuceneManager extends AbstractContentLuceneManager {
	public CorpusLuceneManager(final ComponentParameter compParameter, final File indexPath) {
		super(compParameter, indexPath);
	}

	@Override
	protected String[] getQueryParserFields() {
		return new String[] { "name", "description" };
	}

	@Override
	protected void objectToDocument(final Object object, final IndexWriter indexWriter, final Document doc) throws IOException {
		super.objectToDocument(object, indexWriter, doc);
		for (final String field : getQueryParserFields()) {
			try {
				final Object value = BeanUtils.getProperty(object, field);
				if (value != null) {
					doc.add(new Field(field, String.valueOf(value), Store.NO, Index.ANALYZED));
				}
			} catch (final Exception e) {
				logger.warn(e);
			}
		}
	}

	@Override
	protected LuceneQuery<?> createLuceneQuery(final Query query) {
		try {
			if (IDbComponentHandle.Utils.isManager(getComponentParameter())) {
				return super.createLuceneQuery(query);
			}
		} catch (final Exception e) {
		}
		// my
		return super.createLuceneQuery(query);
	}

	@Override
	protected IDataObjectBean queryForObject(Object id) {
		return DocuUtils.applicationModule.getBean(CorpusBean.class, id);
	}

	@Override
	protected IDataObjectQuery<?> getAllData() {
		final ITableEntityManager tMgr = DocuUtils.applicationModule.getDataObjectManager(CorpusBean.class);
		return tMgr.query(new ExpressionValue("1=1"), CorpusBean.class);
	}
}
