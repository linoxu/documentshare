package net.documentshare.docu.corpus;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.documentshare.docu.AbstractDocuTablePagerData;
import net.documentshare.docu.DocuBean;
import net.documentshare.docu.DocuCatalog;
import net.documentshare.docu.DocuUtils;
import net.documentshare.docu.EDocuStatus;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.SQLValue;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.organization.IJob;
import net.simpleframework.organization.IUser;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractTablePagerData;
import net.simpleframework.web.page.component.ui.pager.TablePagerColumn;
import net.simpleframework.web.page.component.ui.pager.db.AbstractDbTablePagerHandle;

public class CorpusManageTable extends AbstractDbTablePagerHandle {
	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("jobView".equals(beanProperty)) {
			return IJob.sj_account_normal;
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(final ComponentParameter compParameter) {
		String corpusId = compParameter.getRequestParameter("corpusId");
		String a = compParameter.getRequestParameter("a");
		IUser user = ItSiteUtil.getLoginUser(compParameter);
		StringBuffer sql = new StringBuffer();
		final List<Object> ol = new ArrayList<Object>();
		if (StringUtils.hasText(a)) {
			sql.append("select d.* from it_docu_documentshare d where ");
			if (!ItSiteUtil.isManage(compParameter, DocuUtils.applicationModule)) {
				sql.append("d.userId=? and ");
				ol.add(user.getId());
			}
			ol.add(EDocuStatus.publish);
			sql.append(" d.status=? and d.id not in(SELECT cd.docId FROM `it_docu_corpus` c,it_docu_corpusofdoc cd where c.id='" + corpusId
					+ "' and cd.CorpusId=c.id)");
		} else {
			sql.append("SELECT d.* FROM it_docu_documentshare d, `it_docu_corpus` c,it_docu_corpusofdoc cd where d.id=cd.docId and c.id='" + corpusId
					+ "' and cd.CorpusId=c.id");
		}
		final ITableEntityManager tMgr = DocuUtils.applicationModule.getDataObjectManager();
		return tMgr.query(new SQLValue(sql.toString(), ol.toArray()), DocuBean.class);
	}

	@Override
	public AbstractTablePagerData createTablePagerData(final ComponentParameter compParameter) {
		final String corpusId = compParameter.getRequestParameter("corpusId");
		final String a = compParameter.getRequestParameter("a");
		final String setFront = (compParameter.getRequestParameter("setFront") == null ? "" : compParameter.getRequestParameter("setFront"));
		return new AbstractDocuTablePagerData(compParameter) {

			@Override
			public Map<String, TablePagerColumn> getTablePagerColumns() {

				Map<String, TablePagerColumn> column = super.getTablePagerColumns();
				if (!StringUtils.hasText(a)) {
					TablePagerColumn action = column.get("action");
					if (!setFront.equals("true")) {
						action.setTitle("移除");
						action.setColumnText("移除");
					} else {
						action.setTitle("封面");
						action.setColumnText("设为封面");
					}
				} else {
					TablePagerColumn action = column.get("action");
					action.setTitle("添加");
					action.setColumnText("添加");
				}
				return column;
			}

			@Override
			protected Map<Object, Object> getRowData(final Object arg0) {
				final DocuBean docuBean = (DocuBean) arg0;
				final Map<Object, Object> rowData = new LinkedHashMap<Object, Object>();
				rowData.put("title", buildTitle(docuBean));
				final DocuCatalog catalog = DocuUtils.applicationModule.getBean(DocuCatalog.class, docuBean.getCatalogId());
				rowData.put("catalogId", catalog == null ? "" : catalog.getText());
				rowData.put("createDate", ConvertUtils.toDateString(docuBean.getCreateDate(), "yyyy-MM-dd HH"));
				if (StringUtils.hasText(a)) {
					rowData.put("action", "<a style='padding:20px;b' href='javascript:void(0)' onclick=$Actions['addDocForCorpus']('docId="
							+ docuBean.getId().getValue() + "&corpusId=" + corpusId + "')>加到文辑</a>");
				} else {
					if (!setFront.equals("true")) {
						rowData.put("action", "<a style='padding:20px;b' href='javascript:void(0)' onclick=$Actions['removeDocForCorpus']('docId="
								+ docuBean.getId().getValue() + "&corpusId=" + corpusId + "')>移除</a>");
					} else {
						rowData.put("action", "<a style='padding:20px;b' href='javascript:void(0)' onclick=$Actions['setFrontAjax']('frontCoverId="
								+ docuBean.getId().getValue() + "&corpusId=" + corpusId + "')>设为封面</a>");
					}
				}
				return rowData;
			}
		};
	}

	@Override
	public Map<String, Object> getFormParameters(ComponentParameter compParameter) {
		final String corpusId = compParameter.getRequestParameter("corpusId");
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		parameters.put("corpusId", corpusId);
		final String a = compParameter.getRequestParameter("a");
		if (StringUtils.hasText(a)) {
			parameters.put("a", a);
		}
		String setFront=compParameter.getRequestParameter("setFront") ;
		if (StringUtils.hasText(setFront)) {
			parameters.put("setFront",setFront);
		}
		return parameters;
	}
}
