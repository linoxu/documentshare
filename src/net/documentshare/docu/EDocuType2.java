package net.documentshare.docu;

public enum EDocuType2 {
	news() {
		@Override
		public String toString() {
			return "最新";
		}
	},
	popular() {
		@Override
		public String toString() {
			return "最多阅读";
		}
	},
	download() {
		@Override
		public String toString() {
			return "最多下载";
		}

	},
	attention() {
		@Override
		public String toString() {
			return "最多收藏";
		}
	},
	grade() {
		@Override
		public String toString() {
			return "最高评价";
		}
	},
	vote() {
		@Override
		public String toString() {
			return "最高投票";
		}
	},
	remark() {
		@Override
		public String toString() {
			return "最新评论";
		}
	},
	recommended() {
		@Override
		public String toString() {
			return "推荐文档";
		}
	}

}
