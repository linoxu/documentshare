package net.documentshare.question;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.documentshare.mytag.MyTagRBean;
import net.documentshare.mytag.MyTagUtils;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.SQLValue;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.organization.account.AccountSession;
import net.simpleframework.organization.account.IAccount;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.WebUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractPagerHandle;

public class QuestionPaperHandle extends AbstractPagerHandle {
	@Override
	public Object getBeanProperty(final ComponentParameter compParameter, final String beanProperty) {
		return super.getBeanProperty(compParameter, beanProperty);
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(ComponentParameter compParameter) {
		final String c = WebUtils.toLocaleString(compParameter.getRequestParameter("c"));
		if (StringUtils.hasText(c)) {
			return QuestionUtils.applicationModule.createLuceneManager(compParameter).getLuceneQuery(c);
		}
		final IAccount login = AccountSession.getLogin(compParameter.getSession());
		final ITableEntityManager tMgr = QuestionUtils.applicationModule.getDataObjectManager();
		final String projectId = compParameter.getRequestParameter("projectId");
		final String _cs_topic = compParameter.getRequestParameter("_q_topic");
		final StringBuffer sql = new StringBuffer();
		final StringBuffer where = new StringBuffer();
		final List<Object> lv = new ArrayList<Object>();
		sql.append("select q.* from ");
		sql.append(QuestionApplicationModule.question.getName()).append(" q ");
		final String tagId = compParameter.getRequestParameter("tagId");
		if (StringUtils.hasText(tagId)) {
			sql.append(" left join ").append(MyTagUtils.getTableEntityManager(MyTagRBean.class).getTablename()).append(" b");
			sql.append(" on q.id = b.rid ");
			where.append(" and  b.tagid=? ");
			lv.add(tagId);
		}
		if (StringUtils.hasText(projectId)) {
			sql.append(" left join ").append(QuestionApplicationModule.question_project.getName()).append(" qp");
			sql.append(" on q.id=qp.questionId ");
			where.append(" and qp.projectId=? ");
			lv.add(projectId);
		}
		final String t = StringUtils.text(compParameter.getRequestParameter("t"), "open");
		final String od = StringUtils.text(compParameter.getRequestParameter("od"), "new");
		if ("attention".equals(t)) {
			if (login == null) {
				return null;
			}
			sql.append("left join ").append(QuestionApplicationModule.question_attention.getName()).append(" qa ");
			sql.append("on q.id=qa.questionId ");
			where.append(" and q.id=qa.questionId");
			where.append(" and qa.userId=?");
			lv.add(login.getId());
		} else if ("my".equals(t)) {
			if (login == null) {
				return null;
			}
			where.append(" and q.userId=").append(login.getId());
		} else if ("closed".equals(t)) {
			where.append(" and q.status<>?");
			lv.add(EQuestionStatus.noraml);
		} else if ("open".equals(t)) {
			where.append(" and q.status=?");
			lv.add(EQuestionStatus.noraml);
		}
		if ("zero".equals(od)) {
			where.append(" and q.remarks=0");
		}
		if (StringUtils.hasText(_cs_topic)) {
			where.append(" and q.title like ?");
			lv.add("%" + _cs_topic + "%");
		}
		sql.append("where 1=1 ").append(where);
		sql.append(" order by ttop desc");// 按置顶排序
		if ("popular".equals(od)) {
			sql.append(",views desc");
		} else if ("new".equals(od)) {
			sql.append(",modifyDate desc");
		} else if ("vote".equals(od)) {
			sql.append(",votes desc");
		} else if ("remark".equals(od)) {
			sql.append(",remarkDate desc");
		}
		return tMgr.query(new SQLValue(sql.toString(), lv.toArray()), QuestionBean.class);
	}

	@Override
	public Map<String, Object> getFormParameters(ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, "s");
		putParameter(compParameter, parameters, "t");
		putParameter(compParameter, parameters, "zo");
		putParameter(compParameter, parameters, "c");
		putParameter(compParameter, parameters, "_q_topic");
		putParameter(compParameter, parameters, "tagId");

		putParameter(compParameter, parameters, "projectId");
		return parameters;
	}

}
