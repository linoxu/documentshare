package net.documentshare.common;

import java.util.Map;

import net.simpleframework.organization.IJob;
import net.simpleframework.web.page.IForward;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.base.ajaxrequest.AbstractAjaxRequestHandle;

public class CommonPagerAction extends AbstractAjaxRequestHandle {
	@Override
	public Object getBeanProperty(final ComponentParameter compParameter, final String beanProperty) {
		if ("true".equals(compParameter.getRequestParameter("v"))) {
			return super.getBeanProperty(compParameter, beanProperty);
		} else if ("jobExecute".equals(beanProperty)) {
			if (compParameter.componentBean != null) {
				return IJob.sj_account_normal;
			}
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	/**
	 * 公告管理
	 * @param compParameter
	 * @return
	 */
	public IForward notify(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				CommonBean commonBean = CommonUtils.setContent(ECommonType.notify, compParameter.getRequestParameter("notifyId"));
				json.put("rs", commonBean.getContent());
			}
		});
	}

	/**
	 * 预览
	 * @param compParameter
	 * @return
	 */
	public IForward notifyShow(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				json.put("rs", compParameter.getRequestParameter("notifyId"));
			}
		});
	}
}
