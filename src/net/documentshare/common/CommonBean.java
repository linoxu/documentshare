package net.documentshare.common;

import net.simpleframework.core.bean.AbstractIdDataObjectBean;

/**
 *
 */
public class CommonBean extends AbstractIdDataObjectBean {
	private ECommonType type;//类型
	private String content;//内容

	public ECommonType getType() {
		return type;
	}

	public void setType(ECommonType type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
