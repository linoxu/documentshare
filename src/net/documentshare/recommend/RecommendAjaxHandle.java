package net.documentshare.recommend;

import java.util.Map;

import net.documentshare.docu.DocuApplicationModule;
import net.documentshare.docu.DocuCatalogTemp;
import net.documentshare.docu.DocuCodeCatalogTemp;
import net.documentshare.docu.DocuUtils;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.SQLValue;
import net.simpleframework.organization.IJob;
import net.simpleframework.web.page.IForward;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.base.ajaxrequest.AbstractAjaxRequestHandle;

public class RecommendAjaxHandle extends AbstractAjaxRequestHandle {

	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("jobExecute".equals(beanProperty)) {
			return IJob.sj_manager;
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	public IForward revertCatalog(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				final String id = compParameter.getRequestParameter("id");
				if ("docu".equals(id)) {
					final ITableEntityManager tMgr = DocuUtils.applicationModule.getDataObjectManager(DocuCatalogTemp.class);
					tMgr.execute(new SQLValue("delete from " + DocuApplicationModule.docu_catalog_temp.getName()));
					tMgr.execute(new SQLValue("insert into " + DocuApplicationModule.docu_catalog_temp.getName() + " select * from "
							+ DocuApplicationModule.docu_catalog.getName()));
				} else if ("code".equals(id)) {
					final ITableEntityManager tMgr = DocuUtils.applicationModule.getDataObjectManager(DocuCodeCatalogTemp.class);
					tMgr.execute(new SQLValue("delete from " + DocuApplicationModule.code_catalog_temp.getName()));
					tMgr.execute(new SQLValue("insert into " + DocuApplicationModule.code_catalog_temp.getName() + " select * from "
							+ DocuApplicationModule.code_catalog.getName()));
				}
				json.put("rs", id);
			}
		});
	}
}
