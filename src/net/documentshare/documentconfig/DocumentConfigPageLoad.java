package net.documentshare.documentconfig;

import java.util.List;
import java.util.Map;

import net.simpleframework.web.AbstractTitleAwarePageLoad;
import net.simpleframework.web.page.PageParameter;

public class DocumentConfigPageLoad extends AbstractTitleAwarePageLoad {

	public void storageLoad(final PageParameter pageParameter, final Map<String, Object> dataBinding, final List<String> visibleToggleSelector,
			final List<String> readonlySelector, final List<String> disabledSelector) throws Exception {
		final StorageBean storageBean = DocumentConfigMgr.getDocuMgr().getStorageMap().get(pageParameter.getRequestParameter("id"));
		if (storageBean != null) {
			if ("local".equals(storageBean.getType())) {
				dataBinding.put("d_", ((LocalStorageBean) storageBean).getPath());
			} else {
				dataBinding.put("host_", ((FtpStorageBean) storageBean).getHost());
				dataBinding.put("port_", ((FtpStorageBean) storageBean).getPort());
				dataBinding.put("user_", ((FtpStorageBean) storageBean).getUser());
				dataBinding.put("pass_", ((FtpStorageBean) storageBean).getPass());
				dataBinding.put("url_", ((FtpStorageBean) storageBean).getUrl());
				dataBinding.put("path_", ((FtpStorageBean) storageBean).getPath());
			}
			dataBinding.put("id_", storageBean.getId());
		}
	}

}
