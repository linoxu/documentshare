package net.documentshare.utils.doc;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

public class DocViewerConverter {
	private static OfficeConverter officeConverter = new OfficeConverter();
	private static PDFConverter pdfConverter = new PDFConverter();;

	public static File deploy(File file, String outPath) {
		String fileName = FileUtils.getFilePrefix(file);
		File dir = new File(FileUtils.appendFileSeparator(outPath) + fileName);
		if (dir.isFile()) {
			dir.renameTo(new File(dir.getPath() + ".backup"));
		} else {
			dir.mkdirs();
		}

		return dir;
	}

	/**
	 * 转换为swf
	 * 
	 * @param file
	 * @param outPath
	 * @return 返回转换后输出文件目录
	 * @throws Exception
	 */
	public static void toSwf(ConvertParam param) throws Exception {
		boolean isPdf = true;// 当前文件如果是pdf文件为true
		File pdf = param.pdfFile;
		try {
			if (pdf != null) {
				param.poly2bitmap = false;
				pdfConverter.convert(param);
			}
		} catch (Exception e) {
		} finally {
		}
	}

	public static File toPDF(File file, String outPath) throws Exception {
		try {
			String suffix = FileUtils.getFileSuffix(file);
			if (StringUtils.isBlank(suffix)) {
				throw new Exception("The file not has a suffix!");
			}
			if (FileUtils.isPrefix(file, "pdf")) {
				return file;
			}
			File pdf = null;
			File dir = new File(outPath);
			pdf = new File(FileUtils.appendFileSeparator(dir.getPath()) + FileUtils.getFilePrefix(file) + ".pdf");
			if (!pdf.exists()) {
				pdf = officeConverter.toPDF(file, dir.getPath());
			}
			return pdf;
		} finally {
		}
	}
}
