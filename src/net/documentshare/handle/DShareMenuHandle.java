package net.documentshare.handle;

import java.util.ArrayList;
import java.util.Collection;

import net.simpleframework.organization.IJob;
import net.simpleframework.organization.OrgUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.menu.AbstractMenuHandle;
import net.simpleframework.web.page.component.ui.menu.MenuBean;
import net.simpleframework.web.page.component.ui.menu.MenuItem;

public class DShareMenuHandle extends AbstractMenuHandle {

	@Override
	public Collection<MenuItem> getMenuItems(final ComponentParameter compParameter,
			final MenuItem menuItem) {
		if (menuItem != null) {
			return null;
		}
		final MenuBean menuBean = (MenuBean) compParameter.componentBean;
		final ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>(menuBean.getMenuItems());
		if (!OrgUtils.isMember(IJob.sj_manager, compParameter.getSession())) {
			menuItems.remove(menuItems.size() - 1);
		}
		return menuItems;
	}
}
