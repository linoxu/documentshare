package net.documentshare.mytag;

import net.simpleframework.content.EContentType;
import net.simpleframework.core.bean.AbstractIdDataObjectBean;
import net.simpleframework.core.bean.IViewsBeanAware;
import net.simpleframework.core.id.ID;

public class MyTagBean extends AbstractIdDataObjectBean implements IViewsBeanAware {
	private ETagType vtype; // bbs=0 blog=1 news=2

	private ID catalogId;

	private String tagText;

	private int frequency;

	private long views;

	private EContentType ttype;

	public void setVtype(ETagType vtype) {
		this.vtype = vtype;
	}

	public ETagType getVtype() {
		return vtype;
	}

	public ID getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(final ID catalogId) {
		this.catalogId = catalogId;
	}

	public String getTagText() {
		return tagText;
	}

	public void setTagText(final String tagText) {
		this.tagText = tagText;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(final int frequency) {
		this.frequency = frequency;
	}

	@Override
	public long getViews() {
		return views;
	}

	@Override
	public void setViews(final long views) {
		this.views = views;
	}

	public EContentType getTtype() {
		return ttype == null ? EContentType.normal : ttype;
	}

	public void setTtype(final EContentType ttype) {
		this.ttype = ttype;
	}

	private static final long serialVersionUID = 4643467385781286595L;
}
