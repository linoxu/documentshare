package net.simpleframework.swing;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

import net.simpleframework.util.LocaleI18n;

import org.jdesktop.swingx.JXButton;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public final class ErrorDialog extends EnhancedDialog {
	private final class CloseButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent evt) {
			dispose();
		}
	}

	class MessagePanel extends MultipleLineLabel {
		private static final long serialVersionUID = -5726168439328783811L;

		MessagePanel(final String msg) {
			super();
			setText(msg);
			setBackground(ErrorDialog.getTextAreaBackgroundColor());
			setRows(3);
			final Dimension dim = getPreferredSize();
			dim.width = PREFERRED_WIDTH;
			setPreferredSize(dim);
		}
	}

	private final class MoreButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent evt) {
			stackTraceScroller.setVisible(!stackTraceScroller.isVisible());
			moreBtn.setText(stackTraceScroller.isVisible() ? LESS : MORE);
			pack();
		}
	}

	class StackTracePanel extends MultipleLineLabel {
		private static final long serialVersionUID = -7232774230107142648L;

		StackTracePanel(final Throwable th) {
			super();
			setBackground(ErrorDialog.getTextAreaBackgroundColor());
			if (th != null) {
				setText(getStackTrace(th));
				setRows(10);
			}
		}

		private String getStackTrace(final Throwable th) {
			if (th == null) {
				throw new IllegalArgumentException("Throwable == null");
			}

			final StringWriter sw = new StringWriter();
			try {
				final PrintWriter pw = new PrintWriter(sw);
				try {
					th.printStackTrace(pw);
					return sw.toString();
				} finally {
					pw.close();
				}
			} finally {
				try {
					sw.close();
				} catch (final IOException ignore) {
				}
			}
		}
	}

	private static final String ERROR = LocaleI18n.getMessage("ErrorDialog.0");

	private static final String UNKNOWN_ERROR = LocaleI18n.getMessage("ErrorDialog.1");

	private static final String MORE = LocaleI18n.getMessage("ErrorDialog.2");

	private static final String LESS = LocaleI18n.getMessage("ErrorDialog.3");

	private static final String CLOSE = LocaleI18n.getMessage("ErrorDialog.4");

	private static final int PREFERRED_WIDTH = 400;

	private static final long serialVersionUID = -8288638855166328904L;

	private static Color getTextAreaBackgroundColor() {
		return (Color) UIManager.get("TextArea.background");
	}

	private JButton closeBtn;

	private final ActionListener closeHandler = new CloseButtonHandler();

	private JButton moreBtn;

	private final ActionListener moreHandler = null;

	private JScrollPane stackTraceScroller;

	public ErrorDialog(final Window owner, final String msg) {
		super(owner, ERROR, msg, null);
	}

	public ErrorDialog(final Window owner, final String msg, final Throwable th) {
		super(owner, ERROR, msg, th);
	}

	public ErrorDialog(final Window owner, final Throwable th) {
		super(owner, ERROR, null, th);
	}

	@Override
	protected void createUI() {
		String msg = (String) elements[0];
		final Throwable th = (Throwable) elements[1];
		if ((msg == null) || (msg.length() == 0)) {
			if (th != null) {
				msg = th.getMessage();
				if ((msg == null) || (msg.length() == 0)) {
					msg = th.toString();
				}
			}
		}
		if ((msg == null) || (msg.length() == 0)) {
			msg = UNKNOWN_ERROR;
		}

		final MessagePanel msgPnl = new MessagePanel(msg);
		stackTraceScroller = new JScrollPane(new StackTracePanel(th));
		stackTraceScroller.setVisible(false);

		final JPanel btnsPnl = new JPanel();
		if (th != null) {
			moreBtn = new JXButton(MORE);
			btnsPnl.add(moreBtn);
			moreBtn.addActionListener(new MoreButtonHandler());
		}
		closeBtn = new JXButton(CLOSE);
		closeBtn.addActionListener(new CloseButtonHandler());
		btnsPnl.add(closeBtn);

		final Container content = getContentPane();
		content.setLayout(new GridBagLayout());
		final GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(4, 4, 4, 4);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		content.add(new JScrollPane(msgPnl), gbc);
		++gbc.gridy;
		content.add(btnsPnl, gbc);
		++gbc.gridy;
		content.add(stackTraceScroller, gbc);
		setResizable(false);

		SwingUtils.initComponentHeight(moreBtn, closeBtn);
	}

	@Override
	public void dispose() {
		if ((closeBtn != null) && (closeHandler != null)) {
			closeBtn.removeActionListener(closeHandler);
		}
		if ((moreBtn != null) && (moreHandler != null)) {
			moreBtn.removeActionListener(moreHandler);
		}
		super.dispose();
	}

	@Override
	public void ok() {
	}
}
