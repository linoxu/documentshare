package net.simpleframework.swing;

import org.jdesktop.swingx.JXFrame;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class JFrameEx extends JXFrame {

	private static final long serialVersionUID = 2539536733006079160L;

	public JFrameEx() {
	}
}
