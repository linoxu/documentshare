package net.simpleframework.swing;

import java.awt.event.ActionEvent;
import java.util.EventObject;
import java.util.HashSet;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.SortOrder;

import net.simpleframework.swing.JTableExDialogs.ColumnControlDialog;
import net.simpleframework.swing.JTableExDialogs.ExportCSVDialog;
import net.simpleframework.swing.JTableExDialogs.FilterDialog;
import net.simpleframework.swing.JTableExDialogs.FunctionValueDialog;
import net.simpleframework.swing.JTableExDialogs.TableStatDialog;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class JTableExActions extends JActions {
	private final JTableEx table;

	private Action defaultDeleteAction, defaultEditAction, defaultRefreshAction;

	private Action defaultCopyAction;

	public JTableExActions(final JTableEx table) {
		this.table = table;
	}

	Action getDeleteAction() {
		if (defaultDeleteAction == null) {
			defaultDeleteAction = getAction(table, "#(JTableExActions.0)", "tbl-delete.png",
					table.getDeleteActionCallback());
			defaultDeleteAction.putValue(Action.MNEMONIC_KEY, (int) 'D');
		}
		return defaultDeleteAction;
	}

	Action getEditAction() {
		if (defaultEditAction == null) {
			defaultEditAction = getAction(table, "#(JTableExActions.1)", "tbl-edit.png",
					table.getEditActionCallback());
			defaultEditAction.putValue(Action.MNEMONIC_KEY, (int) 'E');
		}
		return defaultEditAction;
	}

	Action getRefreshAction() {
		if (defaultRefreshAction == null) {
			defaultRefreshAction = getAction(table, "#(JTableExActions.2)", "refresh.png",
					new ActionCallback() {
						@Override
						public void doAction() {
							table.refreshTableData();
						}
					});
			defaultRefreshAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("F5"));
		}
		return defaultRefreshAction;
	}

	Action getCopyAction() {
		if (defaultCopyAction == null) {
			defaultCopyAction = getAction(table, "#(JTableExActions.3)", "tbl-copy.png",
					new ActionCallback() {
						@Override
						public void doAction() {
							table.doCopy();
						}
					});
			defaultCopyAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl C"));
		}
		return defaultCopyAction;
	}

	private Action defaultFindAction;

	Action getFindAction() {
		if (defaultFindAction == null) {
			defaultFindAction = getAction(table, "#(JTableExActions.20)", "tbl-find.png",
					new ActionCallback() {
						@Override
						public void doAction() {
							table.getActionMap().get("find")
									.actionPerformed((ActionEvent) getEventObject());
						}
					});
		}
		return defaultFindAction;
	}

	private Action defaultHorizontalScrollAction;

	Action getHorizontalScrollAction() {
		if (defaultHorizontalScrollAction == null) {
			defaultHorizontalScrollAction = getAction(table, null, null, new ActionCallback() {
				@Override
				public void doAction() {
					table.setHorizontalScrollEnabled(!table.isHorizontalScrollEnabled());
				}
			});
		}
		return defaultHorizontalScrollAction;
	}

	private Action defaultTableStatisticsAction, defaultExportCSVAction;

	Action getTableStatisticsAction() {
		if (defaultTableStatisticsAction == null) {
			defaultTableStatisticsAction = getAction(table, "#(JTableExActions.4)", "tbl-attri.png",
					new ActionCallback() {
						@Override
						public void doAction() {
							new TableStatDialog(table);
						}

						@Override
						public boolean executeInQueue() {
							return true;
						}
					});
			defaultTableStatisticsAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("S"));
		}
		return defaultTableStatisticsAction;
	}

	Action getExportCSVActionAction() {
		if (defaultExportCSVAction == null) {
			defaultExportCSVAction = getAction(table, "#(JTableExActions.5)", "tbl-csv.png",
					new ActionCallback() {
						@Override
						public void doAction() {
							new ExportCSVDialog(table);
						}

						@Override
						public boolean executeInQueue() {
							return true;
						}
					});
			defaultExportCSVAction.putValue(Action.MNEMONIC_KEY, (int) 'V');
		}
		return defaultExportCSVAction;
	}

	private Action defaultFetchSizeAction;

	Action getFetchSizeAction() {
		if (defaultFetchSizeAction == null) {
			defaultFetchSizeAction = getAction(table, null, null, new ActionCallback() {
				@Override
				public void doAction() {
					final JQuerySetTable<?> qst = (JQuerySetTable<?>) table;
					final EventObject eventObject = getEventObject();
					if (eventObject instanceof ActionEvent) {
						final String t = ((AbstractButton) ((ActionEvent) eventObject).getSource())
								.getText();
						if ("100".equals(t)) {
							qst.setDataFetchSize(100, true);
						} else if ("200".equals(t)) {
							qst.setDataFetchSize(200, true);
						} else if ("500".equals(t)) {
							qst.setDataFetchSize(500, true);
						} else if ("1000".equals(t)) {
							qst.setDataFetchSize(1000, true);
						} else if ("2000".equals(t)) {
							qst.setDataFetchSize(2000, true);
						} else {
							qst.setDataFetchSize(0, true);
						}
					}
				}
			});
		}
		return defaultFetchSizeAction;
	}

	private Action defaultFilterAction, defaultDeleteFilterAction;

	Action getFilterAction() {
		if (defaultFilterAction == null) {
			defaultFilterAction = getAction(table, "#(JTableExActions.6)", "tbl-filter.png",
					new ActionCallback() {
						@Override
						public void doAction() {
							new FilterDialog(table);
						}

						@Override
						public boolean executeInQueue() {
							return true;
						}
					});
		}
		return defaultFilterAction;
	}

	Action getDeleteFilterAction() {
		if (defaultDeleteFilterAction == null) {
			defaultDeleteFilterAction = getAction(table, "#(JTableExActions.7)",
					"tbl-filter-delete.png", new ActionCallback() {
						@Override
						public void doAction() {
							final JTableExColumn tc = table.mTableExColumn;
							if (tc.getFilterExpression() == null) {
								return;
							}
							table.doFilter(tc, null);
							table.getTableHeader().updateUI();
						}
					});
		}
		return defaultDeleteFilterAction;
	}

	private Action defaultSortASCAction, defaultSortDESCAction, defaultClearSortAction;

	private Action defaultFunctionSumAction, defaultFunctionAvgAction, defaultFunctionMaxAction,
			defaultFunctionMinAction;

	private class SortActionCallback extends ActionCallback {
		private final SortOrder order;

		private SortActionCallback(final SortOrder order) {
			this.order = order;
		}

		@Override
		public void doAction() {
			table.setSortOrder(table.convertColumnIndexToView(table.mTableExColumn.getModelIndex()),
					order);
		}

		@Override
		public boolean executeInQueue() {
			if (table instanceof JQuerySetTable<?>) {
				if (!((JQuerySetTable<?>) table).isDatabaseSorter()) {
					return true;
				}
			}
			return super.executeInQueue();
		}
	}

	Action getSortASCAction() {
		if (defaultSortASCAction == null) {
			defaultSortASCAction = getAction(table, "#(JTableExActions.8)", "tbl-sort-asc.png",
					new SortActionCallback(SortOrder.ASCENDING));
		}
		return defaultSortASCAction;
	}

	Action getSortDESCAction() {
		if (defaultSortDESCAction == null) {
			defaultSortDESCAction = getAction(table, "#(JTableExActions.9)", "tbl-sort-desc.png",
					new SortActionCallback(SortOrder.DESCENDING));
		}
		return defaultSortDESCAction;
	}

	Action getClearSortAction() {
		if (defaultClearSortAction == null) {
			defaultClearSortAction = getAction(table, "#(JTableExActions.10)", "tbl-no-sort.png",
					new SortActionCallback(SortOrder.UNSORTED));
		}
		return defaultClearSortAction;
	}

	private class FunctionAction extends ActionCallback {
		private final String function;

		private FunctionAction(final String function) {
			this.function = function;
		}

		@Override
		public void doAction() {
			new FunctionValueDialog(table, function);
		}

		@Override
		public boolean executeInQueue() {
			return true;
		}
	}

	Action getFunctionSumAction() {
		if (defaultFunctionSumAction == null) {
			defaultFunctionSumAction = getAction(table, "#(JTableExActions.11)", null,
					new FunctionAction("sum"));
		}
		return defaultFunctionSumAction;
	}

	Action getFunctionAvgAction() {
		if (defaultFunctionAvgAction == null) {
			defaultFunctionAvgAction = getAction(table, "#(JTableExActions.12)", null,
					new FunctionAction("avg"));
		}
		return defaultFunctionAvgAction;
	}

	Action getFunctionMaxAction() {
		if (defaultFunctionMaxAction == null) {
			defaultFunctionMaxAction = getAction(table, "#(JTableExActions.13)", null,
					new FunctionAction("max"));
		}
		return defaultFunctionMaxAction;
	}

	Action getFunctionMinAction() {
		if (defaultFunctionMinAction == null) {
			defaultFunctionMinAction = getAction(table, "#(JTableExActions.14)", null,
					new FunctionAction("min"));
		}
		return defaultFunctionMinAction;
	}

	private Action defaultPackAllAction, defaultFreezableAction;

	Action getPackAllAction() {
		if (defaultPackAllAction == null) {
			defaultPackAllAction = getAction(table, "#(JTableExActions.19)", null,
					new ActionCallback() {
						@Override
						public void doAction() {
							table.packAll();
						}
					});
		}
		return defaultPackAllAction;
	}

	Action getFreezableAction() {
		if (defaultFreezableAction == null) {
			defaultFreezableAction = getAction(table, null, "tbl-lock.png", new ActionCallback() {
				@Override
				public void doAction() {
					final HashSet<JTableExColumn> hs = new HashSet<JTableExColumn>();
					for (final int col : table.getSelectedColumns()) {
						final JTableExColumn column = table.getPrivateTableExColumn(col);
						if (column != null) {
							hs.add(column);
						}
					}
					if (table.mTableExColumn != null) {
						hs.add(table.mTableExColumn);
					}
					table.setColumnFreezable(hs.toArray(new JTableExColumn[hs.size()]));
				}

				@Override
				public boolean executeInQueue() {
					return true;
				}
			});
		}
		return defaultFreezableAction;
	}

	private Action defaultColumnControlAction;

	Action getColumnControlAction() {
		if (defaultColumnControlAction == null) {
			defaultColumnControlAction = getAction(table, null, "table.png", new ActionCallback() {
				@Override
				public void doAction() {
					new ColumnControlDialog(table);
				}

				@Override
				public boolean executeInQueue() {
					return true;
				}
			});
		}
		return defaultColumnControlAction;
	}
}
