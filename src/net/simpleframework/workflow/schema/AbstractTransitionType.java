package net.simpleframework.workflow.schema;

import org.dom4j.Element;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class AbstractTransitionType extends AbstractNode {
	private boolean manual;

	public AbstractTransitionType(final Element dom4jElement, final TransitionNode parent) {
		super(dom4jElement, parent);
	}

	public boolean isManual() {
		return manual;
	}

	public void setManual(final boolean manual) {
		this.manual = manual;
	}

	public static class Conditional extends AbstractTransitionType {
		private String expression;

		public Conditional(final Element element, final TransitionNode parent) {
			super(element == null ? addTransitionType(parent, "conditional") : element, parent);
		}

		public String getExpression() {
			return expression;
		}

		public void setExpression(final String expression) {
			this.expression = expression;
		}
	}

	public static class LogicConditional extends AbstractTransitionType {
		private ETransitionLogic logic;

		private String transitionId;

		public LogicConditional(final Element element, final TransitionNode parent) {
			super(element == null ? addTransitionType(parent, "logic-conditional") : element, parent);
		}

		public ETransitionLogic getLogic() {
			return logic;
		}

		public void setLogic(final ETransitionLogic logic) {
			this.logic = logic;
		}

		public String getTransitionId() {
			return transitionId;
		}

		public void setTransitionId(final String transitionId) {
			this.transitionId = transitionId;
		}
	}

	public static class Interface extends AbstractTransitionType {
		private String handleClass;

		public Interface(final Element element, final TransitionNode parent) {
			super(element == null ? addTransitionType(parent, "interface") : element, parent);
		}

		public String getHandleClass() {
			return handleClass;
		}

		public void setHandleClass(final String handleClass) {
			this.handleClass = handleClass;
		}
	}

	static Element addTransitionType(final TransitionNode transition, final String name) {
		return addElement(transition, "transition-type", name);
	}
}
