package net.simpleframework.workflow.schema;

import java.util.Collection;

import org.dom4j.Element;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class UserNode extends AbstractTaskNode {

	private AbstractUserNodeParticipantType participantType;

	private Collection<VariableNode> variables;

	public UserNode(final Element element, final ProcessNode processNode) {
		super(element == null ? addNode(processNode, "user-node") : element, processNode);
	}

	public AbstractUserNodeParticipantType getParticipantType() {
		if (participantType == null) {
			participantType = new AbstractUserNodeParticipantType.User(null, this);
		}
		return participantType;
	}

	public void setParticipantType(final AbstractUserNodeParticipantType participantType) {
		this.participantType = participantType;
	}

	public Collection<VariableNode> variables() {
		return variables;
	}

	@Override
	public void syncElement() {
		super.syncElement();
		getParticipantType().syncElement();
	}

	@Override
	public void parseElement() {
		super.parseElement();
		final Element ele = getElement().element("participant-type");
		if (ele == null) {
			return;
		}
		Element ele2;
		if ((ele2 = ele.element("job")) != null) {
			participantType = new AbstractUserNodeParticipantType.Job(ele2, this);
		} else if ((ele2 = ele.element("user")) != null) {
			participantType = new AbstractUserNodeParticipantType.User(ele2, this);
		}
	}
}
