package net.simpleframework.my.message;

import net.simpleframework.ado.DataObjectManagerUtils;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.util.HTMLUtils;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.PageRequestResponse;
import net.simpleframework.web.page.component.ui.tabs.TabHref;
import net.simpleframework.web.page.component.ui.tabs.TabsUtils;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public abstract class MessageUtils {
	public static IMessageApplicationModule applicationModule;

	public static String deployPath;

	public static String getCssPath(final PageRequestResponse requestResponse) {
		final StringBuilder sb = new StringBuilder();
		sb.append(deployPath).append("css/").append(applicationModule.getSkin(requestResponse));
		return sb.toString();
	}

	public static ITableEntityManager getTableEntityManager(final Class<?> beanClazz) {
		return DataObjectManagerUtils.getTableEntityManager(applicationModule, beanClazz);
	}

	public static ITableEntityManager getTableEntityManager() {
		return DataObjectManagerUtils.getTableEntityManager(applicationModule);
	}

	public static String getTextBody(final String textBody) {
		String c = StringUtils.blank(textBody);
		c = HTMLUtils.htmlEscape(c);
		c = HTMLUtils.autoLink(c);
		c = HTMLUtils.convertHtmlLines(c);
		return c;
	}

	public static String tabs(final PageRequestResponse requestResponse) {
		return TabsUtils
				.tabs(requestResponse,
						new TabHref("#(MessageUtils.0)", applicationModule
								.getApplicationUrl(requestResponse)), new TabHref("#(MessageUtils.1)",
								applicationModule.getSystemMessageUrl(requestResponse)));
	}
}
