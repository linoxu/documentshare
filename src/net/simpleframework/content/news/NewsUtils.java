package net.simpleframework.content.news;

import java.util.ArrayList;

import net.simpleframework.ado.DataObjectManagerUtils;
import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.content.ContentLayoutUtils;
import net.simpleframework.content.EContentStatus;
import net.simpleframework.content.EContentType;
import net.simpleframework.content.component.newspager.NewsPagerUtils;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.util.DateUtils;
import net.simpleframework.util.DateUtils.TimeDistance;
import net.simpleframework.util.HTTPUtils;
import net.simpleframework.web.EFunctionModule;
import net.simpleframework.web.page.PageRequestResponse;
import net.simpleframework.web.page.component.ComponentParameter;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public abstract class NewsUtils {
	public static INewsApplicationModule applicationModule;

	public static String deployPath;

	public static String getCssPath(final PageRequestResponse requestResponse) {
		final StringBuilder sb = new StringBuilder();
		sb.append(deployPath).append("css/").append(applicationModule.getSkin(requestResponse));
		return sb.toString();
	}

	public static ITableEntityManager getTableEntityManager(final Class<?> beanClazz) {
		return DataObjectManagerUtils.getTableEntityManager(applicationModule, beanClazz);
	}

	public static ITableEntityManager getTableEntityManager() {
		return DataObjectManagerUtils.getTableEntityManager(applicationModule);
	}

	public static String getTemplatePage(final ComponentParameter compParameter) {
		if (compParameter.componentBean == null) {
			compParameter.componentBean = applicationModule.getComponentBean(compParameter);
			HTTPUtils.putParameter(compParameter.request, NewsPagerUtils.BEAN_ID,
					compParameter.componentBean.hashId());
		}
		return applicationModule.getTemplatePage(compParameter);
	}

	public static IDataObjectQuery<News> queryNews(final PageRequestResponse requestResponse,
			final Object catalog, final EContentType contentType, final TimeDistance timeDistance,
			final int order) {
		final ArrayList<Object> al = new ArrayList<Object>();
		final StringBuilder sql = new StringBuilder();
		sql.append("status=?");
		al.add(EContentStatus.publish);
		if (catalog != null) {
			sql.append(" and catalogid=?");
			al.add(catalog);
		}
		if (timeDistance != null) {
			sql.append(" and createdate>?");
			al.add(DateUtils.getTimeCalendar(timeDistance).getTime());
		}
		if (contentType != null) {
			sql.append(" and ttype=?");
			al.add(contentType);
		}
		if (order == 0) {
			sql.append(" order by createdate desc");
		} else if (order == 1) {
			sql.append(" order by views desc");
		}
		return getTableEntityManager(News.class).query(
				new ExpressionValue(sql.toString(), al.toArray()), News.class);
	}

	public static IDataObjectQuery<NewsRemark> queryRemarks(final PageRequestResponse requestResponse) {
		return ContentLayoutUtils.getQueryByExpression(requestResponse, applicationModule,
				NewsRemark.class, new ExpressionValue("1=1 order by createdate desc"));
	}

	static void doStatRebuild() {
		NewsPagerUtils.doNewsStatRebuild(applicationModule, EFunctionModule.news);
	}

	/**************************** jsp utils ****************************/
}
