package net.simpleframework.content.blog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.applets.attention.AttentionUtils;
import net.simpleframework.content.ContentUtils;
import net.simpleframework.content.EContentType;
import net.simpleframework.content.component.newspager.NewsPagerUtils;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.organization.IUser;
import net.simpleframework.organization.OrgUtils;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.HTMLBuilder;
import net.simpleframework.util.LocaleI18n;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.EFunctionModule;
import net.simpleframework.web.IWebApplicationModule;
import net.simpleframework.web.page.PageRequestResponse;
import net.simpleframework.web.page.PageUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.menu.MenuBean;
import net.simpleframework.web.page.component.ui.menu.MenuItem;
import net.simpleframework.web.page.component.ui.pager.AbstractTablePagerData;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class BlogPagerHandle extends AbstractBlogPagerHandle {

	@Override
	public String getNavigateHTML(final ComponentParameter compParameter) {
		final StringBuilder sb = new StringBuilder();
		sb.append(super.getNavigateHTML(compParameter));
		final Blog blog = getEntityBeanByRequest(compParameter);
		if (blog != null) {
			final IUser user = OrgUtils.um().queryForObjectById(blog.getUserId());
			if (user != null) {
				sb.append(HTMLBuilder.NAV);
				sb.append("<a href=\"").append(getApplicationModule().getBlogUrl(compParameter, user))
						.append("\">");
				sb.append(user.getText()).append("</a>");
			}
			sb.append(HTMLBuilder.NAV);
			sb.append(LocaleI18n.getMessage("DefaultNewsPagerHandle.0"));
		}
		wrapNavImage(compParameter, sb);
		return sb.toString();
	}

	@Override
	public List<MenuItem> getHeaderMenu(final ComponentParameter compParameter,
			final MenuBean menuBean) {
		return doManagerHeaderMenu(compParameter, menuBean, this, "blogManagerToolsWindow");
	}

	@Override
	public Map<String, Object> getFormParameters(final ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, "t");
		return parameters;
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(final ComponentParameter compParameter) {
		final String c = PageUtils.toLocaleString(compParameter.getRequestParameter("c"));
		if (StringUtils.hasText(c)) {
			return createLuceneManager(compParameter).getLuceneQuery(c);
		} else {
			final ITableEntityManager blog_mgr = getTableEntityManager(compParameter);
			final StringBuilder sql = new StringBuilder();
			final ArrayList<Object> al = new ArrayList<Object>();
			final String t = compParameter.getRequestParameter("t");
			if ("attention".equals(t)) {
				return blog_mgr.query(
						AttentionUtils.attentionSQLValue(compParameter, getFunctionModule()),
						getEntityBeanClass());
			}
			if ("recommended".equals(t)) {
				sql.append("ttype=?");
				al.add(EContentType.recommended);
			} else {
				sql.append("1=1");
			}
			sql.append(" order by ttop desc, createdate desc");
			final ExpressionValue ev2 = new ExpressionValue(sql.toString(), al.toArray());
			return blog_mgr.query(ev2, getEntityBeanClass());
		}
	}

	@Override
	protected boolean isEditable(final PageRequestResponse requestResponse) {
		return IWebApplicationModule.Utils.isManager(requestResponse, BlogUtils.applicationModule);
	}

	@Override
	public AbstractTablePagerData createTablePagerData(final ComponentParameter compParameter) {
		return new BlogTablePagerData(compParameter) {
			@Override
			protected String getTitle(final Blog blog) {
				final StringBuilder sb = new StringBuilder();
				sb.append("<div class=\"f3\" style=\"margin: 4px 0px;\">");
				sb.append(wrapOpenLink(compParameter, blog));
				sb.append("</div>");
				sb.append("<table cellpadding=\"0\" cellspacing=\"0\"><tr>");
				sb.append("<td width=\"80px;\" valign=\"top\">");
				sb.append(ContentUtils.getAccountAware().wrapImgAccountHref(compParameter,
						blog.getUserId()));
				sb.append("</td>");
				sb.append("<td>");
				sb.append("<div class=\"gray-color\">")
						.append(ContentUtils.getShortContent(blog, 300, true)).append("</div>");
				sb.append("<div class=\"hsep\">");
				sb.append(ContentUtils.getAccountAware().wrapAccountHref(compParameter,
						blog.getUserId(), blog.getUserText()));
				sb.append(" , ").append(ConvertUtils.toDateString(blog.getCreateDate()));
				sb.append(getImageByType(blog.getTtype()));
				sb.append(NewsPagerUtils.getNewsContentUrl(compParameter, blog));
				if ("attention".equals(compParameter.getRequestParameter("t"))) {
					sb.append(deleteAttention(EFunctionModule.blog, blog));
				}
				sb.append("</div>");
				sb.append("</td>");
				sb.append("</tr></table>");
				return sb.toString();
			}
		};
	}
}
