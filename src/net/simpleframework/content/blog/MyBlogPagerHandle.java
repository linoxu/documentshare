package net.simpleframework.content.blog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.content.ContentUtils;
import net.simpleframework.content.component.newspager.NewsPagerUtils;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.organization.IUser;
import net.simpleframework.organization.OrgUtils;
import net.simpleframework.organization.account.IAccount;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.HTMLBuilder;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.IWebApplicationModule;
import net.simpleframework.web.page.PageRequestResponse;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.menu.MenuBean;
import net.simpleframework.web.page.component.ui.menu.MenuItem;
import net.simpleframework.web.page.component.ui.pager.AbstractTablePagerData;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class MyBlogPagerHandle extends AbstractBlogPagerHandle {

	@Override
	public String getNavigateHTML(final ComponentParameter compParameter) {
		final StringBuilder sb = new StringBuilder();
		sb.append(super.getNavigateHTML(compParameter));
		final IAccount account = ContentUtils.getAccountAware().getAccount(compParameter);
		if (account != null) {
			final IUser user = account.user();
			if (user != null) {
				sb.append(HTMLBuilder.NAV);
				sb.append("<a href=\"")
						.append(BlogUtils.applicationModule.getBlogUrl(compParameter, user))
						.append("\">").append(user.getText()).append("</a>");
			}
			final BlogCatalog blogCatalog = BlogCatalogHandle.getBlogCatalogByRequest(compParameter);
			if (blogCatalog != null) {
				sb.append(HTMLBuilder.NAV).append(blogCatalog.getText());
			}
		}
		wrapNavImage(compParameter, sb);
		return sb.toString();
	}

	private List<MenuItem> contextMenu;

	@Override
	public List<MenuItem> getContextMenu(final ComponentParameter compParameter,
			final MenuBean menuBean) {
		if (contextMenu == null) {
			contextMenu = new ArrayList<MenuItem>(super.getContextMenu(compParameter, menuBean));
			final ArrayList<MenuItem> removes = new ArrayList<MenuItem>();
			for (final MenuItem item : contextMenu) {
				final String jsCallback = item.getJsSelectCallback();
				if (StringUtils.hasText(jsCallback) && jsCallback.contains(".edit2(")) {
					removes.add(item);
				}
			}
			contextMenu.removeAll(removes);
		}
		return contextMenu;
	}

	@Override
	public Map<String, Object> getFormParameters(final ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, OrgUtils.um().getUserIdParameterName());
		return parameters;
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(final ComponentParameter compParameter) {
		final StringBuilder sql = new StringBuilder();
		final ArrayList<Object> al = new ArrayList<Object>();
		final IAccount login = ContentUtils.getAccountAware().getAccount(compParameter);
		if (login == null) {
			sql.append("1=2");
		} else {
			sql.append("userid=?");
			al.add(login.getId());
			final BlogCatalog blogCatalog = BlogCatalogHandle.getBlogCatalogByRequest(compParameter);
			if (blogCatalog != null) {
				sql.append(" and catalogid=?");
				al.add(blogCatalog.getId());
			}
		}
		sql.append(" order by ttop desc, createdate desc");
		final ExpressionValue ev2 = new ExpressionValue(sql.toString(), al.toArray());
		return getTableEntityManager(compParameter).query(ev2, getEntityBeanClass());
	}

	@Override
	protected boolean isEditable(final PageRequestResponse requestResponse) {
		return IWebApplicationModule.Utils.isManager(requestResponse, BlogUtils.applicationModule)
				|| ContentUtils.getAccountAware().isMyAccount(requestResponse);
	}

	@Override
	public AbstractTablePagerData createTablePagerData(final ComponentParameter compParameter) {
		return new BlogTablePagerData(compParameter) {
			@Override
			protected String getTitle(final Blog blog) {
				final StringBuilder sb = new StringBuilder();
				sb.append("<table cellpadding=\"0\" cellspacing=\"0\"><tr>");
				sb.append("<td width=\"10\" style=\"padding-left: 0px;\" valign=\"top\">");
				if (isEditable(compParameter)) {
					sb.append("<span class=\"drag_image\" style=\"margin-top: 9px;\"></span>");
				}
				sb.append("</td><td>");
				sb.append("<div class=\"f3\" style=\"margin: 4px 0px;\">");
				final BlogCatalog blogCatalog = BlogUtils.getTableEntityManager(BlogCatalog.class)
						.queryForObjectById(blog.getCatalogId(), BlogCatalog.class);
				if (blogCatalog != null) {
					sb.append("<a onclick=\"$Actions['__my_blog_pager']('catalog=");
					sb.append(blogCatalog.getId()).append("');\">[");
					sb.append(blogCatalog.getText()).append("]</a> ");
				}
				sb.append(wrapOpenLink(compParameter, blog)).append("</div>");
				sb.append("<table cellpadding=\"0\" cellspacing=\"0\"><tr>");
				final String img = ContentUtils.getContentImage(compParameter, blog);
				if (img != null) {
					sb.append("<td width=\"110\" valign=\"top\">").append(img).append("</td>");
				}
				sb.append("<td valign=\"top\">");
				sb.append("<div class=\"gray-color\">")
						.append(ContentUtils.getShortContent(blog, 300, true)).append("</div>");
				sb.append("</td></tr></table>");
				sb.append("<div class=\"hsep\">").append(
						ConvertUtils.toDateString(blog.getCreateDate()));
				sb.append(NewsPagerUtils.getNewsContentUrl(compParameter, blog));
				sb.append("</div>");
				sb.append("</td></tr></table>");
				return sb.toString();
			}
		};
	}

	@Override
	public String getJavascriptCallback(final ComponentParameter compParameter,
			final String jsAction, final Object bean) {
		String jsCallback = super.getJavascriptCallback(compParameter, jsAction, bean);
		if ("delete".equals(jsAction)) {
			jsCallback += "$Actions[\"__my_blog_catalog\"].refresh();";
		}
		return jsCallback;
	}
}
