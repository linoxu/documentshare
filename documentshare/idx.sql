alter table it_docu_documentshare add success tinyint(4) default 0;


CREATE TABLE `simple_vote` (  
`id` bigint(20) NOT NULL, 
 `documentid` bigint(20) NOT NULL,  
`text` varchar(80) DEFAULT NULL,
`description` varchar(256) DEFAULT NULL, 
`expireddate` date DEFAULT NULL,
`createdate` datetime NOT NULL, 
`userid` bigint(20) DEFAULT NULL, 
`lastupdate` datetime DEFAULT NULL, 
`lastuserid` bigint(20) DEFAULT NULL, 
`status` tinyint(4) NOT NULL,  `oorder`
bigint(20) NOT NULL,  
PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
