<%@page import="net.simpleframework.web.WebUtils"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="net.simpleframework.web.page.component.ui.pager.PagerUtils"%>
<%@ page import="java.util.List"%><%@page
	import="net.documentshare.question.QuestionBean"%><%@page
	import="net.documentshare.question.QuestionUtils"%><%@page
	import="net.documentshare.question.QuestionExtBean"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.simpleframework.util.DateUtils"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final QuestionBean questionBean = QuestionUtils.getQuestionBean(requestResponse);
%>
<style>
.pager .pager_top_block {
	margin-top: 10px;
	border-bottom: 1px solid #ccc;
}

.pager .pager_top_block .pager_title {
	margin-left: 3px;
	height: 20px;
	font-family: verdana;
}

.pager .pager_head {
	display: none;
}

#_tablepaper .titem {
	display: inline-block;
	vertical-align: top;
	border-bottom: 1px solid #EEE;
	margin-top: -1px;
	min-height: 20px;
	width: 99%;
}

#_tablepaper .titem:HOVER {
	background-color: #F8F8F8;
}
</style>
<div id="_tablepaper">
	<%
		final List<?> data = PagerUtils.getPagerList(request);
		if (data == null && data.size() == 0) {
			return;
		}
		for (Object o : data) {
			final QuestionExtBean questionExtBean = (QuestionExtBean) o;
	%>
	<div class="titem" align="left">
		<table style="width: 100%">
			<tr>
				<td class="title">
					<span><%=questionExtBean.getContent()%></span>
					<%
						if (ItSiteUtil.isManageOrSelf(requestResponse, QuestionUtils.applicationModule, questionBean.getUserId()))
								out.println("<a onclick=\"$Actions['questionExtDeleteAct']('questionExtId=" + questionExtBean.getId() + "');\">删除</a>");
					%>
				</td>
				<td align="right"><%=DateUtils.getRelativeDate(questionExtBean.getCreateDate())%></td>
			</tr>
		</table>
	</div>
	<%
		}
	%>
</div>
