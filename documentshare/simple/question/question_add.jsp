<%@page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@page
	import="net.simpleframework.web.page.component.ComponentRenderUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.documentshare.question.QuestionUtils"%>


<%
	final ComponentParameter nComponentParameter = new ComponentParameter(request, response, null);
%>
<style>
#td_q_limitTime .text,#td_q_limitTime .text input,#__np__newsAddForm #q_p1,#__np__newsAddForm #q_p2,#__np__newsAddForm #q_p3,#__np__newsAddForm #q_title,#__np__newsAddForm #q_reward,#__np__newsAddForm #q_keywords
	{
	border-width: 0;
	width: 99%;
	background-image: none;
}

.gssb_d {
	position: absolute;
	z-index: 9999;
	margin-left: -2px;
	border-bottom: 1px solid #E7E7E7;
	border-left: 1px solid #E7E7E7;
	border-right: 1px solid #E7E7E7;
	border-left: 1px solid #E7E7E7;
	border-bottom: 1px solid #E7E7E7;
	border-left: 1px solid #E7E7E7;
}
</style>
<div id="__np__newsAddForm">
	<%=ComponentRenderUtils.genParameters(nComponentParameter)%>
	<input type="hidden" id="questionId" name="questionId"
		value="<%=nComponentParameter.getRequestParameter(QuestionUtils.questionId)%>">
	<table cellspacing="0" class="tbl tbl_first">
		<tr>
			<td class="lbl">
				主题
			</td>
			<td>
				<input type="text" id="q_title" name="q_title" />
			</td>
		</tr>
	</table>
	<table cellspacing="0" class="tbl">
		<tr>
			<td class="lbl">
				关键字
			</td>
			<td>
				<input type="text" id="q_keywords" name="q_keywords" />
			</td>
			<td class="lbl2" style="width: 80px;">
				赠送积分
			</td>
			<td>
				<input type="text" " id="q_reward" name="q_reward" value="0">
			</td>
			<td class="lbl2" style="width: 80px;">
				结束时间
			</td>

			<td>
				<div id="td_q_limitTime"></div>
			</td>
		</tr>
	</table>
	<table class="tbl" cellspacing="0">
		<tr>
			<td class="lbl">
				内容
			</td>
			<td style="padding: 3px;">
				<div class="clear_float" style="padding-bottom: 3px;">
					<div style="float: right;"><%=ItSiteUtil.getHtmlEditorToolbar(nComponentParameter, "question", "q_content")%></div>
					<div style="float: left" id="q_content_info" class="important-tip">
						#(CKEditor.0)
					</div>
				</div>
				<div>
					<textarea id="q_content" name="q_content" style="display: none;"></textarea>
				</div>
			</td>
		</tr>
	</table>
	<table class="tbl" cellspacing="0">
		<tr>
			<td class="lbl">
				验证
			</td>
			<td>
				<div class="clear_float" style="padding: 4px;">
					<div style="float: right;">
						<div class="btn">
							<table>
								<tr>
									<td></td>
									<td>
										<input type="button" class="button2" value="保存"
											id="__questionAdd" onclick="$IT.A('questionAddAct');" />
										<input type="button" value="取消"
											onclick="$Actions['questionAddWindowAct'].close();" />
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div style="float: left;" id="questionEditorValidateCode"></div>
					<div style="float: left;">
						<input type="checkbox" id="q_email" name="q_email" value="true">
						<label for="q_email">
							有人回答时邮件通知我
						</label>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>
<script type="text/javascript">
(function() {
	addTextButton("q_limitTime", "limitTimeCalAct", "td_q_limitTime", true);
	window.onbeforeunload = function() {
		return "#(CKEditor.1)";
	};
})();
</script>
