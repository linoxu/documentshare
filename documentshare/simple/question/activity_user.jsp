<%@page import="net.simpleframework.content.ContentUtils"%>
<%@page import="net.simpleframework.organization.account.IAccount"%>
<%@page import="net.simpleframework.organization.OrgUtils"%>
<%@page
	import="net.simpleframework.web.page.component.ui.pager.PagerUtils"%>
<%@page import="net.simpleframework.organization.impl.User"%>
<%@page import="java.util.Map"%>
<%@page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.core.ado.IDataObjectQuery"%>
<%@ page import="net.simpleframework.util.ConvertUtils"%>
<%@ page import="net.simpleframework.content.ContentLayoutUtils"%><%@page
	import="net.documentshare.question.QuestionUtils"%><%@page import="net.documentshare.utils.ItSiteUtil"%>



<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final IDataObjectQuery<?> qs = QuestionUtils.queryQuestion(requestResponse, "activity");
	if (qs == null) {
		return;
	}
%>
<style>
.activity_user {
	width: 52px;
	display: inline-block;
	padding-right: 6px;
}

.activity_user .user_a {
	width: 56px;
	display: block; /*内联对象需加*/
	word-break: keep-all; /* 不换行 */
	white-space: nowrap; /* 强制在同一行内显示所有文本，直到文本结束或者遭遇 br 对象。不换行 */
	overflow: hidden; /* 内容超出宽度时隐藏超出部分的内容 */
	text-overflow: ellipsis;
	display: block;
	/* IE 专有属性，当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用。*/
}
</style>
<%
	Map<String, Object> mapData;
	while ((mapData = (Map<String, Object>) qs.next()) != null) {
		if (qs.position() >= 32) {
			break;
		}
		IAccount account = ItSiteUtil.getAccountById(mapData.get("userId"));
		if (account == null)
			continue;
%>
<table class="activity_user">
	<tr>
		<td title="<%=account.user().getText()%>"><%=ContentUtils.getAccountAware().wrapImgAccountHref(requestResponse, account.user(), 44, 44)%>
		</td>
	</tr>
	<tr>
		<td class="user_a" align="center"
			style="overflow: hidden; font-size: 7pt;">
			<span><%=ContentUtils.getAccountAware().wrapAccountHref(requestResponse, account.getId(), account.user().getText())%></span>
		</td>
	</tr>
</table>
<%
	}
%>
