<%@page import="net.simpleframework.web.WebUtils"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="net.simpleframework.web.page.component.ui.pager.PagerUtils"%>
<%@ page import="java.util.List"%>
<%@page import="net.simpleframework.util.HTMLBuilder"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.documentshare.question.QuestionBean"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.documentshare.question.QuestionUtils"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final String t = request.getParameter("t");
	final String mark = WebUtils.toLocaleString(StringUtils.text(request.getParameter("_q_topic"), request.getParameter("c")));
%>
<style>
.pager .pager_top_block .pager_title {
	font-size: 11pt;
	margin-left: 3px;
}

.pager .pager_head {
	margin-right: 3px;
}

#_tablepaper .titem {
	display: inline-block;
	vertical-align: top;
	border-bottom: 1px solid #EEE;
	margin-top: -1px;
	width: 99%;
}

#_tablepaper .titem:HOVER {
	background-color: #F8F8F8;
}

#_tablepaper .title {
	font-weight: bold;
}

.pager .pager_top_block .pager_title {
	height: 20px;
}
</style>
<div id="_tablepaper">
	<%
		final List<?> data = PagerUtils.getPagerList(request);
		if (data == null && data.size() == 0) {
			return;
		}
		for (Object o : data) {
			final QuestionBean questionBean = (QuestionBean) o;
	%>
	<div class="titem" rowid="<%=questionBean.getId()%>">
		<table cellpadding="4" style="width: 100%">
			<tr>
				<td valign="top" width="100%">
					<table style="width: 100%">
						<tr>
							<td style="width: 50px;">
								<img alt="积分" src="/simple/question/score.gif">
								<%=questionBean.getReward()%></td>
							<td class="title">
								<a
									href="<%=QuestionUtils.applicationModule.getViewUrl(questionBean.getId())%>"
									target="_blank"> <%=ItSiteUtil.markContent(mark, questionBean.getTitle())%></a>
							</td>
							<td nowrap="nowrap" align="right"><%=ItSiteUtil.layoutRemarkAndViewAndVote(questionBean)%>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<%=ItSiteUtil.layoutRemarkTime(requestResponse, questionBean)%>
								<a class="a2" style="margin-left: 5px;" target="_blank"
									href="<%=QuestionUtils.applicationModule.getViewUrl(questionBean.getId())%>">»阅读全文</a>
								<%
									if ("my".equals(t))
											out.println("<a class=\"qM down_menu_image\"  href=\"javascript:void(0);\"></a>");
										else if ("attention".equals(t))
											out.println("<a class=\"a2 nav_arrow\" onclick=\"$Actions['questionAttention1Act']('questionId=" + questionBean.getId()
													+ "');\">取消关注</a>");
										else if (ItSiteUtil.isManage(requestResponse, QuestionUtils.applicationModule))
											out.println("<a class=\"q2 down_menu_image\" href=\"javascript:void(0);\"></a>");
								%>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<%
		}
	%>
</div>
