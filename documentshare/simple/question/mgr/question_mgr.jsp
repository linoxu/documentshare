<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div style="padding: 0px 12px;">
	<p>
		<input type="button" value="重构统计数据" />
		<span id="span_questionReBuildStatData" class="important-tip"
			style="margin-left: 6px;"></span>
	</p>
	<p>
		<input type="button" value="重建索引" />
		<span id="span_questionIndexRebuild" class="important-tip"
			style="margin-left: 6px;"></span>
	</p>
</div>
<script type="text/javascript">
(function() {
	function init(act) {
		var info = $("span_" + act);
		info.previous().observe("click", function() {
			info.innerHTML = "#(manager_tools.7)";
			$Actions[act]();
		});
	}
	init("questionReBuildStatData");
	init("questionIndexRebuild");
})();
</script>