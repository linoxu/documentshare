<%@page import="net.simpleframework.content.news.NewsUtils"%>
<%@page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.ConvertUtils"%>
<%@ page import="net.simpleframework.content.ContentLayoutUtils"%><%@page
	import="net.simpleframework.core.ado.IDataObjectQuery"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.simpleframework.util.DateUtils"%>


<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final String type = request.getParameter("type");
	final IDataObjectQuery<?> qs = DocuUtils.queryDocu(requestResponse, null,"docu" ,type);
%>
<div class="list_layout">
	<%
		DocuBean docuBean;
		while ((docuBean = (DocuBean) qs.next()) != null) {
			if (qs.position() > 6) {
				break;
			}
	%>
	<div class="rrow" style="padding-left: 0px;">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20" valign="top">
					<img alt="<%=docuBean.getExtension()%>"
						src="<%=DocuUtils.getFileImage(requestResponse, docuBean)%>">
				</td>
				<td><%=DocuUtils.wrapOpenLink(requestResponse, docuBean)%></td>
				<td align="right" class="nnum"  valign="top">
					<%
						if ("new".equals(type)) {
								out.println(DateUtils.getRelativeDate(docuBean.getCreateDate()));
							} else if ("download".equals(type)) {
								out.println(docuBean.getDownCounter());
							} else if ("grade".equals(type)) {
								out.println(docuBean.getTotalGrade());
							}
					%>
				</td>
			</tr>
		</table>
	</div>
	<%
		}
	%>
</div>