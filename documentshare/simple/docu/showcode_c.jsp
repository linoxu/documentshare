<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page import="net.simpleframework.util.HTMLBuilder"%><%@page import="net.simpleframework.web.WebUtils"%>


<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final DocuBean docuBean = DocuUtils.applicationModule.getViewDocuBean(requestResponse);
	if (docuBean == null) {
		return;
	}
%>
<div id="show_code_sub" class="show_template">
	<div class="nav2 clear_float">
		<div style="float: left;">
			<div class="nav1_image">
				<%=docuBean.getDocuFunction()%><%=HTMLBuilder.NAV %><%=DocuUtils.wrapOpenLink(requestResponse,docuBean) %><%=HTMLBuilder.NAV %><%=WebUtils.toLocaleString(request.getParameter("path")) %>
			</div>
		</div>
	</div>
	<div><%=DocuUtils.showCode(requestResponse, docuBean)%></div>
</div>
