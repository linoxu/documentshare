<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.simpleframework.my.file.MyFileUtils"%><%@page
	import="net.documentshare.docu.EDocuType1"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.simpleframework.util.ConvertUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page import="net.simpleframework.web.page.PageRequestResponse"%>

<%
final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	int t = ConvertUtils.toInt(request.getParameter("t"), 0);
	if (t != 0 && !ItSiteUtil.isManage(requestResponse, DocuUtils.applicationModule)) {
		t = 0;
	}
%>
<%
	if (t == EDocuType1.docu.ordinal()) {
%>
<jsp:include page="/simple/template/c.jsp" flush="true">
	<jsp:param value="/simple/docu/docu_c.jsp" name="center" />
</jsp:include>
<%
	} else if (t == EDocuType1.file.ordinal()) {
		final String left = MyFileUtils.deployPath + "jsp/folder_c.jsp";
		final String center = MyFileUtils.deployPath + "jsp/file_c.jsp";
%>
<jsp:include page="/simple/template/c.jsp" flush="true">
	<jsp:param value="/simple/docu/docu_c_file.jsp" name="center" />
</jsp:include>
<%
	} else if (t == EDocuType1.remark.ordinal()) {
%>
<jsp:include page="/simple/template/c.jsp" flush="true">
	<jsp:param value="/simple/docu/docu_c_remark.jsp" name="center" />
</jsp:include>
<%
	} else {
%>
<jsp:include page="/simple/template/lr.jsp" flush="true">
	<jsp:param value="/simple/docu/docu_c.jsp" name="left" />
	<jsp:param value="/simple/docu/layout_right.jsp" name="center" />
</jsp:include>
<%
	}
%>
