<%@page import="net.simpleframework.content.ContentUtils"%>
<%@page import="net.simpleframework.util.DateUtils"%>
<%@page import="net.documentshare.docu.DocuUtils"%>
<%@page import="net.documentshare.docu.corpus.CorpusBean"%>
<%@page import="net.documentshare.utils.ItSiteUtil"%>
<%@page import="net.simpleframework.organization.IUser"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.documentshare.mytag.ETagType"%><%@page
	import="net.documentshare.mytag.MyTagUtils"%><%@page
	import="net.simpleframework.util.StringUtils"%>
<%@page import="net.simpleframework.organization.OrgUtils"%>
<%
	PageRequestResponse pageParame = new PageRequestResponse(request, response);
	IUser user = ItSiteUtil.getUserById(request.getParameter("userId"));
	String id = request.getParameter("corpusId");
	CorpusBean bean = (CorpusBean) DocuUtils.applicationModule.getBean(CorpusBean.class, id);
%>
<div class="block_layout1">
	<div class="t f4">
		文辑信息
	</div>
	<div class="wrap_text">
		<table width="100%">
			<tr>
				<td style="width: 75px;">
					<%=ContentUtils.getAccountAware().wrapImgAccountHref(pageParame, user, 64, 64)%>
				</td>
				<td valign="top">
					<div>
						<%=ContentUtils.getAccountAware().wrapAccountHref(pageParame, user)%>
					</div>
					<div style="padding-top: 2px;">
						更新于<%=DateUtils.getRelativeDate(bean.getUpdateDate())%></div>
					<div style="padding-top: 10px;">
						<a href="/space/<%=user.getId()%>.html?t=corpus">用户更多文辑</a>
					</div>
				</td>
			</tr>
		</table>

	</div>
</div>
<div class="block_layout1">
	<div class="t f4">
		相关文辑
	</div>
	<div class="wrap_text">
		<jsp:include page="corpus_layout_show.jsp" flush="true">
			<jsp:param value="related" name="type" />
		</jsp:include>
	</div>
</div>
