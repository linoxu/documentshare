<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="net.documentshare.docu.DocuUtils"%>
<%@page import="net.documentshare.docu.DocuDelLog"%><%@page import="net.simpleframework.util.StringUtils"%>

<%
	final DocuDelLog docuBean = DocuUtils.applicationModule.getBean(DocuDelLog.class, request.getParameter("delLogId"));
	if (docuBean == null) {
		return;
	}
%>
<div><%=StringUtils.text(docuBean.getDelReason(),"没有原因")%></div>
