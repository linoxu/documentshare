<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="net.documentshare.docu.DocuUtils"%>
<%@page import="net.documentshare.docu.EDocuFunction"%>
<%@page import="net.simpleframework.util.StringUtils"%>
<%@page import="net.documentshare.docu.EDocuType2"%>
<%@page import="net.simpleframework.util.ConvertUtils"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final String _docu_function = request.getParameter("_docu_function");
	int s = 0;
	if (StringUtils.hasText(_docu_function)) {
		s = EDocuFunction.whichOne(_docu_function).ordinal();
	} else {
		s = ConvertUtils.toInt(request.getParameter("s"), 0);
	}
%>
<div class=common_catalog style="margin-bottom: 6px;">
	<div class="c">
		<%=DocuUtils.catalogView2(requestResponse)%></div>
</div>
<input type="hidden" id="__t" name="__t"
	value="<%=request.getParameter("t")%>">
<%
	if (s == 0) {
%>
<div class=common_catalog style="margin-bottom: 6px;">
	<div class="c">
		<div class="ctitle ctitle_border_radius">
			文档
		</div>
		<div id="docuListId_docu"></div>
	</div>
</div>
<div class=common_catalog style="margin-bottom: 6px;"
	id="docuListId_code_">
	<div class="c">
		<div class="ctitle ctitle_border_radius">
			代码
		</div>
		<div id="docuListId_code"></div>
	</div>
</div>
<div class=common_catalog style="margin-bottom: 6px;"
	id="docuListId_data_">
	<div class="c">
		<div class="ctitle ctitle_border_radius">
			资料
		</div>
		<div id="docuListId_data"></div>
	</div>
</div>
<div class=common_catalog style="margin-bottom: 6px;"
	id="docuListId_tool_">
	<div class="c">
		<div class="ctitle ctitle_border_radius">
			工具
		</div>
		<div id="docuListId_tool"></div>
	</div>
</div>
<script type="text/javascript">
$ready(function() {
	$Actions['docuListPaperAct_docu']('tt=1');
	$Actions['docuListPaperAct_code']('tt=2&show=docuListId_code');
	$Actions['docuListPaperAct_data']('tt=3&show=docuListId_data');
	$Actions['docuListPaperAct_tool']('tt=4&show=docuListId_tool');
});
</script>
<%
	} else {
%>
<div class=common_catalog style="margin-bottom: 6px;">
	<div class="c">
		<div class="ctitle ctitle_border_radius" id="special_docu">
			<%=EDocuFunction.whichOrdinal(s)%>
		</div>
		<div id="docuListId"></div>
	</div>
</div>
<script type="text/javascript">
$ready(function() {
	$Actions['docuListPaperAct']('tt=<%=s%>');
});
</script>
<%
	}
%>
<script type="text/javascript">
(function() {
	$$(".common_catalog .ctitle img").each(function(img) {
		$Comp.imageToggle(img, img.up().next());
	});
})();
</script>
