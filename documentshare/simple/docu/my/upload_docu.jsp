<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.documentshare.docu.EDocuFunction"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.documentshare.docu.DocuBean"%>

<%
	final String docuId = request.getParameter("docuId");
	final boolean hasDocu = StringUtils.hasText(docuId);
	final DocuBean docuBean = DocuUtils.applicationModule.getBean(DocuBean.class, docuId);
%>
<div id="docuForm">
	<input type="hidden" id="docuId" name="docuId">
	<div id="docu_content_" style="display: <%=hasDocu ? "" : "none"%>">
		<table width="100%'">
			<tbody>
				<tr>
					<td class="l">
						标题：
					</td>
					<td>
						<input type="text" id="docu_title" name="docu_title"
							style="width: 90%">
					</td>
				</tr>
				<tr>
					<td class="l" valign="top">
						描述：
					</td>
					<td>
						<textarea id="docu_content" name="docu_content" rows="5"
							style="width: 90%"></textarea>
					</td>
				</tr>
				<tr>
					<td class="l">
						关键字：
					</td>
					<td>
						<input type="text" id="docu_keyworks" name="docu_keyworks"
							style="width: 90%">
						<div id="docu_tag"></div>
					</td>
				</tr>
				<tr>
					<td class="l">
						文档分类：
					</td>
					<td>
						<div id="td_docu_catalog" style="width: 90%"></div>
						<input type="hidden" name="docu_catalog" id="docu_catalog">
					</td>
				</tr>
				<tr style="display: none;" class="code_catalog_id">
					<td class="l">
						编译环境分类：
					</td>
					<td>
						<div id="td_docu_code_catalog" style="width: 90%"></div>
						<input type="hidden" name="docu_code_catalog"
							id="docu_code_catalog">
					</td>
				</tr>
				<tr style="display: none;" class="code_catalog_id">
					<td class="l">
						语言：
					</td>
					<td>
						<select id="code_language" name="code_language">
							<%
								for (String l : DocuUtils.languages) {
							%>
							<option value="<%=l%>"><%=l%></option>
							<%
								}
							%>
						</select>
					</td>
				</tr>
				<tr>
					<td class="l">
						属性：
					</td>
					<td>
						<select id="docu_function" name="docu_function"
							onchange="selectDocuAttr(this);">
							<%
								for (EDocuFunction docu : EDocuFunction.values()) {
									if (docuBean != null && docuBean.getDocuFunction() == EDocuFunction.data && docu == EDocuFunction.docu) {
										continue;
									}
							%>
							<option value="<%=docu.name()%>"><%=docu.toString()%></option>
							<%
								}
							%>
						</select>
					</td>
				</tr>
				<tr class="docu_pages">
					<td class="l">
						预览页数：
					</td>
					<td>
						<input type="text" style="width: 50px" id="docu_free_page"
							name="docu_free_page" value="0">
						<span style="color: red;">0为不限制,或者输入百分比,如20%</span>
					</td>
				</tr>
				<tr>
					<td class="l">
						所需积分：
					</td>
					<td>
						<input type="text" style="width: 50px" id="docu_point"
							name="docu_point" value="0">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div align="center">
	<%
		if (hasDocu) {
	%>
	<input type="button" id="uploadDocuBtn" class="button2"
		onclick="$IT.A('docuSaveAct');" value="提交文档">
	<%
		} else {
	%>
	<div style="color: red;">文件名称已存在，如果内容不一样 请更改名称再上传，或者上传其它文件</div>
	<input type="button" id="uploadDocuBtn" class="button2"
		onclick="$Actions.loc('/space.html?t=document&id=myUpload');"
		value="上传其它文件">
	<%
		}
	%>
</div>
<style type="text/css">
#docu_content_ .l {
	width: 100px;
	text-align: right;
}

#docu_content_ {
	background: #f7f7ff;
	padding: 6px 8px;
}
</style>
<script type="text/javascript">
$ready(function() {
	addTextButton("docu_catalog_text", "docuCatalogAct", "td_docu_catalog",
			false);
	addTextButton("docu_code_catalog_text", "docuCodeCatalogAct",
			"td_docu_code_catalog", false);
	$IT.bindEle('mouseover', '.docu_item', function(t) {
		$('docu_id_now').value = t.id;
	});
});
function selectDocuAttr(t) {
	if ('code' == t.value) {
		$$('.code_catalog_id').each(function(c) {
			c.style.display = '';
		});
	} else {
		$$('.code_catalog_id').each(function(c) {
			c.style.display = 'none';
		});
	}
	if ('docu' == t.value) {
		$$('.docu_pages').each(function(c) {
			c.style.display = '';
		});
	} else {
		$$('.docu_pages').each(function(c) {
			c.style.display = 'none';
		});
	}
}
</script>