<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.content.news.NewsUtils"%>
<%@ page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@ page import="net.simpleframework.util.DateUtils.TimeDistance"%>
<%@ page
	import="net.simpleframework.content.component.newspager.NewsPagerUtils"%>
<%
	final ComponentParameter nComponentParameter = new ComponentParameter(
			request, response, null);
	final String templatePage = NewsUtils
			.getTemplatePage(nComponentParameter);
	final String news_layout = NewsUtils.deployPath
			+ "jsp/news_layout.jsp";
%>
<div class="c_left">
	<jsp:include page="<%=templatePage%>" flush="true"></jsp:include>
</div>
<div class="c_right">
	<div class="block_layout1">
		<div class="t f4">相关资讯</div>
		<div class="wrap_text">
			<%
				request.setAttribute("__qs",
						NewsPagerUtils.queryRelatedNews(nComponentParameter));
			%>
			<jsp:include page="<%=news_layout%>" flush="true">
				<jsp:param value="false" name="showMore" />
				<jsp:param value="dot1" name="dot" />
			</jsp:include></div>
	</div>

	<div class="block_layout1">
		<div class="t f4">一周点击排行</div>
		<div class="wrap_text">
			<%
				request.setAttribute("__qs", NewsUtils.queryNews(
						nComponentParameter, null, null, TimeDistance.week, 1));
			%>
			<jsp:include page="<%=news_layout%>" flush="true">
				<jsp:param value="false" name="showMore" />
				<jsp:param value="dot2" name="dot" />
			</jsp:include></div>
	</div>
</div>
<script type="text/javascript">
	(function() {
		$Comp.fixMaxWidth(".newspager_template .inherit_c", 680);
	})();
</script>