<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.HTMLBuilder"%>
<div class="t_footer2">
	<div class="t">
		友情链接
	</div>
	<div style="padding: 6px;">
		<a href="http://www.itniwo.net/" target="_blank">I++技术网站</a>
	</div>
</div>

<style type="text/css">
.t_footer2 {
	border: 1px solid #ccc;
	margin-top: 8px;
	padding: 2px;
}

.t_footer2 .t {
	font-size: 9.5pt;
	background-color: #f4f4f4;
	text-align: center;
	padding: 6px 0px;
}

.t_footer2 .l {
	color: #B75009;
	font-size: 9.5pt;
	border-bottom: 1px dashed #999;
	width: 65%;
	padding-bottom: 4px;
	margin-bottom: 4px;
}

.t_footer2  a {
	display: table;
	*display: block;
	*margin: 2px 0;
}
</style>