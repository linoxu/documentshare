<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.documentshare.ESearchFunction"%><%@page
	import="net.simpleframework.web.WebUtils"%>

<%
	final String _search_function = request.getParameter("_search_function");
	final String _search_topic = WebUtils.toLocaleString(request.getParameter("_search_topic"));
	final ESearchFunction searchFunction = ESearchFunction.whichSearch(_search_function);
	response.sendRedirect(request.getContextPath()
			+ WebUtils.addParameters(searchFunction.toUrl(), "_search_function=" + _search_function + "&_search_topic=" + _search_topic
					+ "&c=" + _search_topic));
%>
