<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.HTMLBuilder"%>
<%@ page import="net.simpleframework.web.page.PageFilter"%><%@page
	import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%>


<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final String menuName = ItSiteUtil.witchMenu(requestResponse);
%>
<div id="t_footer">
	<div>
		<a href="/guestbook.html">留言</a><%=HTMLBuilder.SEP%><a
			href="/contact.html">联系我们</a><%=HTMLBuilder.SEP%><a
			href="/about.html">关于</a>
	</div>
	<div style="padding-top: 4px;">
		itniwo.net Copyright &copy;2011 版权所有
		[ <span id="pageload_time_id" style="color: red;">0</span>&nbsp;s ]
		<br>
	</div>
</div>
<script type="text/javascript">
$ready(function() {
	$$('.mm').each(function(c) {
		if (c.innerHTML == '<%=menuName%>') {
			c.addClassName('mm1');
			return;
		}
	});
});
$ready(function() {
	$Actions["systemNotificationWindow"].startMessage();
	$("pageload_time_id").innerHTML = document
			.getCookie("<%=PageFilter.PAGELOAD_TIME%>");
});
</script>