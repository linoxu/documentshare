<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.simpleframework.util.ConvertUtils"%><%@page
	import="net.simpleframework.content.ContentUtils"%><%@page
	import="net.simpleframework.web.page.PageRequestResponse"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final String docuId = request.getParameter("docuId");
	final DocuBean docuBean = DocuUtils.applicationModule.getBean(DocuBean.class, docuId);
	if (docuBean != null) {
%>
<div class="simple_toolbar1">
	<span style="font-size: 16px; font-weight: bold;">你要投诉的文档是:</span><span
		style="color: red; font-size: 14px; font-weight: bold;">《<%=docuBean.getTitle()%>》</span>
	<div style="text-indent: 2em;">
		<table style="background: #F1F4FC">
			<tr>
				<td align="right" nowrap="nowrap">
					上传用户：
				</td>
				<td width="100%"><%=ContentUtils.getAccountAware().wrapAccountHref(requestResponse, docuBean.getUserId(), docuBean.getUserText())%></td>
			</tr>
			<tr>
				<td align="right">
					上传时间：
				</td>
				<td><%=ConvertUtils.toDateString(docuBean.getCreateDate(), "yyyy-MM-dd")%></td>
			</tr>
			<tr>
				<td valign="top" align="right">
					文档描述：
				</td>
				<td>
					<%=docuBean.getContent()%>
				</td>
			</tr>
		</table>
	</div>
</div>
<script type="text/javascript">
$ready(function(){
	$Actions['_complaint']();
});
</script>
<%
	}else{
%>
<script type="text/javascript">
$ready(function(){
	$Actions['_guestbook']();
});
</script>
<%} %>
<div id="guestbookForm">
	<input type="hidden" value="<%=StringUtils.text(docuId, "0")%>"
		name="docuId" id="docuId">
	<div id="_guestbook">
	</div>
</div>
