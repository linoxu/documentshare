<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page import="net.simpleframework.my.space.MySpaceUtils"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
%>
<div style="background-color: ">
	<%=MySpaceUtils.applicationModule.tabs(requestResponse)%>
</div>
<%
	final String t = request.getParameter("t");
	if ("blog".equals(t)) {
%><jsp:include page="space_blog.jsp"></jsp:include>
<%
	} else if ("bbs".equals(t)) {
%><jsp:include page="space_bbs.jsp"></jsp:include>
<% 
	} else if ("space".equals(t)) {
%><jsp:include page="space_log.jsp"></jsp:include>
<% 
	} else if ("corpus".equals(t)) {
%>
<jsp:include page="/simple/docu/corpus/mycorpus_list.jsp"></jsp:include>
<%
	} else {
%>
<jsp:include page="/simple/docu/my/mydocu.jsp"></jsp:include>
<%
	}
%>


