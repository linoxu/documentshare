<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.content.EContentType"%>
<%@ page import="net.simpleframework.content.news.NewsUtils"%>
<%@ page import="net.simpleframework.content.news.News"%>
<%@ page import="net.simpleframework.core.ado.IDataObjectQuery"%>
<%@ page
	import="net.simpleframework.content.component.newspager.INewsPagerHandle"%>
<%@ page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@ page import="net.simpleframework.util.ConvertUtils"%>
<%@ page import="java.util.Random"%>
<%
	final ComponentParameter nComponentParameter = new ComponentParameter(request, response, null);
	nComponentParameter.componentBean = NewsUtils.applicationModule.getComponentBean(nComponentParameter);
	if (nComponentParameter.componentBean == null) {
		return;
	}
	final IDataObjectQuery<News> qs = NewsUtils.queryNews(nComponentParameter, null, EContentType.announce, null, 0);
	request.setAttribute("queryNews", qs);
	final INewsPagerHandle nHandle = (INewsPagerHandle) nComponentParameter.getComponentHandle();
	final String img = NewsUtils.deployPath + "images/announce.png";
%>
<div id="_news_announce_marquee">
	<%
		News news;
		while ((news = qs.next()) != null) {
			if (qs.position() > 3) {
				break;
			}
	%>
	<div>
		<img src="<%=img%>">
		<span><%=nHandle.wrapOpenLink(nComponentParameter, news)%></span>
	</div>
	<%
		}
	%>
</div>
<style type="text/css">
#_news_announce_marquee img,#_news_announce_marquee a {
	vertical-align: middle;
}

#_news_announce_marquee img {
	cursor: pointer;
	margin: 0px 2px 0px 6px;
}
</style>